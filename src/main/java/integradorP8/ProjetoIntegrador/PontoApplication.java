package integradorP8.ProjetoIntegrador;

import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;

import integradorP8.ProjetoIntegrador.entity.Role;
import integradorP8.ProjetoIntegrador.entity.Usuario;
import integradorP8.ProjetoIntegrador.repository.RoleRepository;
import integradorP8.ProjetoIntegrador.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class PontoApplication extends SpringBootServletInitializer {

	@Autowired
	UsuarioService usuarioService;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	RoleRepository roleRepository;
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PontoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PontoApplication.class, args);
	}

	@PostConstruct
	public void run() throws NoSuchAlgorithmException{
		Usuario user = new Usuario();
		user.setNome("Menino");
		user.setCpf("25463214589");
		user.setData_nascimento("24/05/1990");
		user.setSexo("M");
		user.setTelefone("83654985632");
		user.setEmail("naninha@gmail.com");
		user.setPassword(bCryptPasswordEncoder.encode("admin"));
		user.setAtivo(true);

		Role role = new Role();
		role.setAuthority("ADM");

		roleRepository.save(role);
		user.addRole(role);
		usuarioService.criarUsuario(user);


	}
}
