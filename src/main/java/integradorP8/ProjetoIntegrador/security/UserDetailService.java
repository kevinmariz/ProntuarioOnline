package integradorP8.ProjetoIntegrador.security;

import integradorP8.ProjetoIntegrador.entity.Usuario;
import integradorP8.ProjetoIntegrador.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailService implements UserDetailsService {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException, BadCredentialsException {
        Usuario user = usuarioRepository.findByCpf(cpf);
        System.out.println(user);
        if(user.getId() == null)
            throw new UsernameNotFoundException("Usuario " + cpf + " não encontrado");
        else if(!user.isAtivo())
            throw new BadCredentialsException("Usuario " + cpf + " não esta ativo");
        return new CustomUserDetails(user);
    }
}
