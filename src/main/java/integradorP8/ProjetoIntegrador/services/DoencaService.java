package integradorP8.ProjetoIntegrador.services;

import integradorP8.ProjetoIntegrador.entity.Doenca;
import integradorP8.ProjetoIntegrador.repository.DoencaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoencaService {

    @Autowired
    DoencaRepository doencaRepository;

    public void criarUsuario(Doenca doenca) {
        Doenca user = doencaRepository.findByNome(doenca.getNome());

        if (user == null)
            doencaRepository.save(doenca);

    }

    public void atulalizarUsuario(Doenca doenca){
        Doenca user = doencaRepository.findById(doenca.getId());

        if (user != null)
            doencaRepository.save(doenca);
    }

    public Doenca buscarUsuario(String nome){
        Doenca usuario = doencaRepository.findByNome(nome);
        if(usuario == null)
            return null;
        return usuario;
    }

}
