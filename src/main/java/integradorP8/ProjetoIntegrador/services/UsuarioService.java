package integradorP8.ProjetoIntegrador.services;

import integradorP8.ProjetoIntegrador.entity.Usuario;
import integradorP8.ProjetoIntegrador.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public void criarUsuario(Usuario usuario) {
        Usuario user = usuarioRepository.findByCpf(usuario.getCpf());

        if (user == null)
            usuarioRepository.save(usuario);

    }

    public void atulalizarUsuario(Usuario usuario){
        Usuario user = usuarioRepository.findById(usuario.getId());

        if (user != null)
            usuarioRepository.save(usuario);
    }

    public Usuario buscarUsuario(String cpf){
        Usuario usuario = usuarioRepository.findByCpf(cpf);
        if(usuario == null)
            return null;
        return usuario;
    }

    public void desativarUsuario(Long id) {
        usuarioRepository.desativar(id);
    }
}
