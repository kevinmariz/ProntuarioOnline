package integradorP8.ProjetoIntegrador.services;

import integradorP8.ProjetoIntegrador.entity.AntecedenteOcupacional;
import integradorP8.ProjetoIntegrador.repository.AntecedenteOcupacionalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AntecedenteOcupacionalService {

    @Autowired
    AntecedenteOcupacionalRepository AntecedenteOcupacionalRepository;

    public void criarUsuario(AntecedenteOcupacional antecedenteOcupacional) {
        AntecedenteOcupacional ocupacional = AntecedenteOcupacionalRepository.findByNome(antecedenteOcupacional.getDescricao());

        if (ocupacional == null)
            AntecedenteOcupacionalRepository.save(antecedenteOcupacional);

    }

    public void atulalizarUsuario(AntecedenteOcupacional antecedenteOcupacional){
        AntecedenteOcupacional ocupacional = AntecedenteOcupacionalRepository.findById(antecedenteOcupacional.getId());

        if (ocupacional != null)
            AntecedenteOcupacionalRepository.save(antecedenteOcupacional);
    }

    public AntecedenteOcupacional buscarUsuario(String nome){
        AntecedenteOcupacional ocupacional = AntecedenteOcupacionalRepository.findByNome(nome);
        if(ocupacional == null)
            return null;
        return ocupacional;
    }

}
