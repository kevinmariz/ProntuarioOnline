package integradorP8.ProjetoIntegrador.repository;

import integradorP8.ProjetoIntegrador.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    public Usuario findById(Long id);
    public Usuario findByNome(String nome);
    public Usuario findByCpf(String email);

}
