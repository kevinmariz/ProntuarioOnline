package integradorP8.ProjetoIntegrador.repository;

import integradorP8.ProjetoIntegrador.entity.AntecedenteOcupacional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AntecedenteOcupacionalRepository extends JpaRepository<AntecedenteOcupacional, Long> {

    public AntecedenteOcupacional findById(Long id);
    public AntecedenteOcupacional findByNome(String nome);

}
