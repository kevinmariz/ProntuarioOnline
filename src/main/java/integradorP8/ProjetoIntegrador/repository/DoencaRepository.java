package integradorP8.ProjetoIntegrador.repository;

import integradorP8.ProjetoIntegrador.entity.Doenca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoencaRepository extends JpaRepository<Doenca, Long> {

    public Doenca findById(Long id);
    public Doenca findByNome(String nome);

}
