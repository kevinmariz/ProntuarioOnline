package integradorP8.ProjetoIntegrador.entity;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper=false)
public @Data class AntecedentesFamiliares extends Anamnese {

	private String parentesco;
	
}
