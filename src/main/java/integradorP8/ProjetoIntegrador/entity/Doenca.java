package integradorP8.ProjetoIntegrador.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

public @Data class Doenca {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_doenca")
	private Long id;
	
	private String nome;
	
	private String descricao;
	
	private boolean tipo;
	
	private boolean status;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_criacao;
	
}
