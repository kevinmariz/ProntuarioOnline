package integradorP8.ProjetoIntegrador.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public @Data class
Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_usuario")
	private Long id;
	
	@Column(name="nome")
	@NotEmpty(message = "Por favor, forneça seu nome")
	private String nome;
	
	@Column(name = "cpf")
	@Length(min = 11, message = "Seu cpf deve conter 11 caracteres")
	@NotEmpty(message = "Por favor, forneça seu cpf")
	private String cpf;
	
	@Column(name="data_nascimento")
	@NotEmpty(message = "Por favor, forneça sua data de Nascimento")
	private String data_nascimento;
	
	@Column(name="sexo")
	private String sexo;
	
	@Column(name="Rg")
	private String rg;
	
	@Column(name="estado_civil")
	private String estado_civil;
	
	@Column(name="unidade_lotacao")
	private String unidade_lotacao;
	
	@Column(name="Cargo")
	private String Cargo;
	
	@Column(name="funcao")
	private String funcao;
	
	@Column(name="telefone")
	@NotEmpty(message = "Por favor, forneça seu telefone")
	private String telefone;
	
	@Column(name="email")
	@Email(message="Por favor fornceça um email válido")
	@NotEmpty(message="Por favor, forneça um e-mail")
	private String email;
	
	@Column(name = "senha")
	@Length(min = 5, message = "Sua senha deve conter pelo menos 5 caracteres")
	@NotEmpty(message = "Por favor, forneça uma senha")
	private String password;
	
	@Column(name = "ativo")
	private boolean ativo = true;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_modificacao;
	
	@OneToMany(mappedBy="usuario", fetch = FetchType.EAGER)
	private List<Anamnese> anamnese;
	
	@OneToMany(mappedBy="usuario", fetch = FetchType.EAGER)
	private List<TipoExames> tipo_exames;
	
	@OneToMany(mappedBy="usuario", fetch = FetchType.EAGER)
	private List<AntecedenteOcupacional> antecedentes_ocupacinais;

	@OneToMany
	private List<Role> roles;

	public void addRole(Role role) {roles.add(role);}


}
