package integradorP8.ProjetoIntegrador.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public @Data class Anamnese {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_anamnese")
	private Long id;
	
	private boolean opcao;
	
	private Doenca doenca;
	
	@ManyToOne
	private Usuario usuario;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_criacao;
	
}
