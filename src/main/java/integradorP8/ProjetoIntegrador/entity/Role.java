package integradorP8.ProjetoIntegrador.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

public @Data class Role implements GrantedAuthority {

    private String authority;

    public String getAuthority() {
        return authority;
    }
}
