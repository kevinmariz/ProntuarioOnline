package integradorP8.ProjetoIntegrador.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

public @Data class ExamesSucessivos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_exames")
	private long id;
	
	private String exames;
	
	private String dados_encontrados;
	
	private String nome_medico;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	
}
