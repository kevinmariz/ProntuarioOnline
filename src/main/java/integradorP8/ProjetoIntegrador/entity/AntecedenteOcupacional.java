package integradorP8.ProjetoIntegrador.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

public @Data class AntecedenteOcupacional {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_ocupacionais")
	private Long id;
	
	private String descricao;
	
	private String opcao;
	
	@Column(name="data_criacao")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	@ManyToOne
	private Usuario usuario;
	
}
