package integradorP8.ProjetoIntegrador.controller;

import integradorP8.ProjetoIntegrador.entity.Usuario;
import integradorP8.ProjetoIntegrador.security.CustomUserDetails;
import integradorP8.ProjetoIntegrador.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

    private UsuarioService usuarioService;

    @Autowired
    public UsuarioController(UsuarioService usuarioService){
        this.usuarioService = usuarioService;
    }

    @RequestMapping(value= "/perfil-prontuario/{cpf}", method = RequestMethod.GET)
    public String perfilMethod(@PathVariable("cpf") String cpf, Model model){
        Usuario usuario = usuarioService.buscarUsuario(cpf);
        model.addAttribute("usuario", usuario);
        return "";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrarMethod(Model model){
        model.addAttribute("usuario", new Usuario());
        return "";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public String cadastrarMethod(@Valid @ModelAttribute Usuario usuario, BindingResult result) {
        if(result.hasErrors())
            return "";
        usuarioService.criarUsuario(usuario);
        return "";
    }

    @RequestMapping(value = "/editarUsuario", method = RequestMethod.POST)
    public String editarUsuarioMethod(@Valid @ModelAttribute Usuario usuario, BindingResult result) {
        if(result.hasErrors())
            return "";
        usuarioService.atulalizarUsuario(usuario);
        return "";
    }

    @RequestMapping(value = "/editarUsuario", method = RequestMethod.POST)
    public String desativarUsuarioMethod(@Valid @ModelAttribute Usuario usuario, @AuthenticationPrincipal CustomUserDetails principal) {
        usuarioService.desativarUsuario(principal.getId());
        return "";
    }


}
