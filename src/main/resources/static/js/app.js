$( document ).ready(function() {

    /*********************************** NAVBAR-SIDEBAR ****************************************/
	
	/* Initialize collapse button (show side-nav)
	   {menuWidth}:{number} // Choose the width of side-nav // Default is 300
	   {edge}:{direction} // Choose the horizontal origin // Default is 'left'
	   {closeOnClick}:{boolean} // Closes side-nav on <a> clicks
	   {draggable}:{boolean} // Choose whether you can drag to open on touch screens */
	 
	$(".button-collapse").sideNav({
	  menuWidth: 200, 
	  edge: 'right',
	  closeOnClick: true,
	  draggable: true
	});

	// Initialize collapsible (uncomment the line below if you use the dropdown variation)
	// $('.collapsible').collapsible();

	/**
	 *   Other Controls
	 */
	// Show sideNav
	// $('.button-collapse').sideNav('show');
	// Hide sideNav
	// $('.button-collapse').sideNav('hide');
	// Destroy sideNav
	// $('.button-collapse').sideNav('destroy')

	/********************************** // ******************************************************/

	/********************************* DATEPICKER-INSCRIÇÃO**************************************/

	$('.datepicker').pickadate({
	    selectMonths: true, // Creates a dropdown to control month
	    selectYears: 15 // Creates a dropdown of 15 years to control year
	  });

	  $(document).ready(function() {
	    $('select').material_select();
	  });

	/********************************** // ******************************************************/

});